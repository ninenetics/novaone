//******************splitting.js************************//
!function(n,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):n.Splitting=t()}(this,function(){"use strict";var n=document,t=n.createTextNode.bind(n);function e(n,t,e){n.style.setProperty(t,e)}function r(n,t){return n.appendChild(t)}function i(t,e,i,u){var o=n.createElement("span");return e&&(o.className=e),i&&(!u&&o.setAttribute("data-"+e,i),o.textContent=i),t&&r(t,o)||o}function u(n,t){return n.getAttribute("data-"+t)}function o(t,e){return t&&0!=t.length?t.nodeName?[t]:[].slice.call(t[0].nodeName?t:(e||n).querySelectorAll(t)):[]}function c(n){for(var t=[];n--;)t[n]=[];return t}function s(n,t){n&&n.some(t)}function a(n){return function(t){return n[t]}}var l={};function f(n,t,e,r){return{by:n,depends:t,key:e,split:r}}function d(n){return function n(t,e,r){var i=r.indexOf(t);if(-1==i)r.unshift(t),s(l[t].depends,function(e){n(e,t,r)});else{var u=r.indexOf(e);r.splice(i,1),r.splice(u,0,t)}return r}(n,0,[]).map(a(l))}function p(n){l[n.by]=n}function h(n,e,u,c,a){n.normalize();var l=[],f=document.createDocumentFragment();c&&l.push(n.previousSibling);var d=[];return o(n.childNodes).some(function(n){if(!n.tagName||n.hasChildNodes()){if(n.childNodes&&n.childNodes.length)return d.push(n),void l.push.apply(l,h(n,e,u,c,a));var r=n.wholeText||"",o=r.trim();o.length&&(" "===r[0]&&d.push(t(" ")),s(o.split(u),function(n,t){t&&a&&d.push(i(f,"whitespace"," ",a));var r=i(f,e,n);l.push(r),d.push(r)})," "===r[r.length-1]&&d.push(t(" ")))}else d.push(n)}),s(d,function(n){r(f,n)}),n.innerHTML="",r(n,f),l}var m="words",g=f(m,0,"word",function(n){return h(n,"word",/\s+/,0,1)}),v="chars",y=f(v,[m],"char",function(n,t,e){var r=[];return s(e[m],function(n,e){r.push.apply(r,h(n,"char","",t.whitespace&&e))}),r});function w(n){var t=(n=n||{}).key;return o(n.target||"[data-splitting]").map(function(r){var i={el:r},o=d(n.by||u(r,"splitting")||v),c=function(n,t){for(var e in t)n[e]=t[e];return n}({},n);return s(o,function(n){if(n.split){var u=n.by,o=(t?"-"+t:"")+n.key,a=n.split(r,c,i);o&&(l=r,p=(d="--"+o)+"-index",s(f=a,function(n,t){Array.isArray(n)?s(n,function(n){e(n,p,t)}):e(n,p,t)}),e(l,d+"-total",f.length)),i[u]=a,r.classList.add(u)}var l,f,d,p}),r.classList.add("splitting"),i})}function b(n,t,e){var r={};return s(o(t.matching||n.children,n),function(n){var t=Math.round(n[e]);(r[t]||(r[t]=[])).push(n)}),Object.keys(r).map(Number).sort().map(a(r))}w.html=function(n){var t=(n=n||{}).target=i();return t.innerHTML=n.content,w(n),t.outerHTML},w.add=p;var N=f("lines",[m],"line",function(n,t,e){return b(n,{matching:e[m]},"offsetTop")}),x=f("items",0,"item",function(n,t){return o(t.matching||n.children,n)}),T=f("rows",0,"row",function(n,t){return b(n,t,"offsetTop")}),L=f("cols",0,"col",function(n,t){return b(n,t,"offsetLeft")}),k=f("grid",["rows","cols"]),A="layout",C=f(A,0,0,function(n,t){var c=t.rows=+(t.rows||u(n,"rows")||1),s=t.columns=+(t.columns||u(n,"columns")||1);if(t.image=t.image||u(n,"image")||n.currentSrc||n.src,t.image){var a=o("img",n)[0];t.image=a&&(a.currentSrc||a.src)}t.image&&e(n,"background-image","url("+t.image+")");for(var l=c*s,f=[],d=i(0,"cell-grid");l--;){var p=i(d,"cell");i(p,"cell-inner"),f.push(p)}return r(n,d),f}),M=f("cellRows",[A],"row",function(n,t,e){var r=t.rows,i=c(r);return s(e[A],function(n,t,e){i[Math.floor(t/(e.length/r))].push(n)}),i}),S=f("cellColumns",[A],"col",function(n,t,e){var r=t.columns,i=c(r);return s(e[A],function(n,t){i[t%r].push(n)}),i}),H=f("cells",["cellRows","cellColumns"],"cell",function(n,t,e){return e[A]});return p(g),p(y),p(N),p(x),p(T),p(L),p(k),p(C),p(M),p(S),p(H),w});
//******************splitting.js*end***********************//
/***************text Splitting************************/
Splitting();
/***************text Splitting*end***********************/
//not in mobile js
function isMobile(){
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
if (!isMobile()){
//if its not mobile then start script otherwise turn it off (just place the script    under this comment
//**************wow js************************//
 new WOW().init();
};
//**********************mobile menu css********************************//
(function($) {
$.fn.menumaker = function(options) {  
 var cssmenu = $(this), settings = $.extend({
   format: "dropdown",
   sticky: false
 }, options);
 return this.each(function() {
   $(this).find("#menu-toggle").on('click', function(){
     $(this).toggleClass('menu-opened');
     var mainmenu = $(this).next('ul');
     if (mainmenu.hasClass('open')) { 
       mainmenu.slideToggle().removeClass('open');
     }
     else {
       mainmenu.slideToggle().addClass('open');
       if (settings.format === "dropdown") {
         mainmenu.find('ul').show();
       }
     }
   });
   cssmenu.find('li ul').parent().addClass('has-sub');
multiTg = function() {
     cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
     cssmenu.find('.submenu-button').on('click', function() {
       $(this).toggleClass('submenu-opened');
       if ($(this).siblings('ul').hasClass('open')) {
         $(this).siblings('ul').removeClass('open').slideToggle();
       }
       else {
         $(this).siblings('ul').addClass('open').slideToggle();
       }
     });
   };
   if (settings.format === 'multitoggle') multiTg();
   else cssmenu.addClass('dropdown');
   if (settings.sticky === true) cssmenu.css('position', 'fixed');
resizeFix = function() {
  var mediasize = 767;
     if ($( window ).width() > mediasize) {
       cssmenu.find('ul').show();
     }
     if ($(window).width() <= mediasize) {
       cssmenu.find('ul').hide().removeClass('open');
     }
   };
   resizeFix();
   return $(window).on('resize', resizeFix);
 });
  };
})(jQuery);

(function($){
$(document).ready(function(){
$("#cssmenu").menumaker({
   format: "multitoggle"
});
});
})(jQuery);

//*******************mob menu btn*************************************//
$('#menu-toggle').click(function(){
  $(this).toggleClass('open');
})

/****************sticky header*********************/
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');
        }
    }
    
    lastScrollTop = st;
}
